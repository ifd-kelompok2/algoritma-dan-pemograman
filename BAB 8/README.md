
# Berikut Soal-soal yang ada di BAB 8 :

1. Buatlah algoritma yang membaca sejumlah karakter dan mencetaknya ke layar. Buatlah algoritma untuk 2 kasus:
    (a) Jumlah karakter yang dibaca diketahui, yaitu N  buah ( baca N  terlebih dahulu).
    (b) Jumlah karakter yang dibaca tidak diketahui tetapi proses pembacaan berakhir jika karakter yang 
        dimasukkan adalah karakter titik (Karakter titik tidak ikut dicetak).

2. Buatlah algoritma untuk menghitung jumlah N   buah bilangan ganjil pertama ( yaitu,  1 + 3 + 5 + …).  
    Catatan: N  adalah  bilangan bulat tidak negatif.

3. Buatlah algoritma untuk menghitung jumlah bilangan ganjil dari 1 sampai N ( yaitu, 1 + 3 + 5 + .. + N).
    (perhatikan perbedaan soal latihan ini dengan soal nomor 2).
 
4. Tulislah program dalam bahasa pascal dan C untuk mencetak segitiga bintang berikut ini Jika diberikan Tinggi 
    segitiga adalah N ( asumsikan N > 0).
    contohnya, jika N  = 5, maka segitiga yang dihasilkan adalah :
    *
    **
    ***
    ****
    ***
    **
    * 

5. Tuliskan algoritma untuk menampilkan semua solusi bilangan bulat tidak negatif dari persamaan berikut:
    x + y + z = 25 yang dalam hal ini x >0,y>0,z > 0.

6. Buatlah algoritma yang akan  mengkonversi bilangan bulat positif  ke angka romawinya. rancanglah algoritma 
    tersebut sehingga pengonversi and tersebut dapat dilakukan berulang kali sampai nilai nol dibaca dari 
    piranti masukan.
 
7. Seseorang mempunyai tabungan di sebuah bank. ia dapat menyetor dan mengambil uangnya di bank tersebut Namun,
    jumlah saldo minimum yang harus disisakan di dalam adalah Rp10.000.  ini artinya, jika saldonya  Rp10.000, 
    ia tidak dapat mengambil uang lagi. kode transaksi untuk menyetor adalah 0 dan kode transaksi untuk 
    mengambil adalah 1.  Buatlah algoritma yang mensimulasikan transaksi yang dilakukan orang tersebut titik 
    algoritma menerima masukan berupa kode Transaksi dan jumlah uang yang  disetor/diambil. rancanglah 
    algoritma tersebut sehingga memungkinkan penabung dapat melakukan kan transaksi berulang kali sampai saldo 
    yang tersisa Rp10.000 atau jumlah uang yang diambil lebih besar dari saldonya.
    Catatan: nilai uang yang diambil selalu merupakan bilangan bulat.
 
8. Barisan Fibonacci dinyatakan dengan rumus Fi = Fi-1 + F-2.  2 bilangan Fibonacci pertama adalah F1 = F2 = 1 
    titik Tulislah algoritma untuk mencetak n bilangan bilangan Fibonacci pertama.
 
9. Tulislah algoritma untuk mengetes Apakah sebuah bilangan bulat n merupakan bilangan prima atau bukan. 
    caranya, bagilah n dengan 2,3,... n.  jika n tidak habis dibagi 2,3,... n, maka n prima.
 
10. Tulislah program pascal/c/c++  untuk menghitung jumlah n bilangan ganjil pertama 
    ( yaitu, 1 + 3 + 5 +...+ 2n-1). 

