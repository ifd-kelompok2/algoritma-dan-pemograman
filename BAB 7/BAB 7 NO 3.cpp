#include <iostream>
using namespace std;
main(){
   int a, b, c, x;
	cout<<" Tulislah algoritma yang membaca tiga buah bilangan bulat,";
    cout<<" lalu mengurutkan tiga buah bilangan tersebut dari nilai yang kecil";
    cout<<" ke nilai yang besar. Keluaran adalah tiga buah bilangan yang terurut"<<endl;
   cout<<"Masukkan a : ";
   cin>>a;
   cout<<"Masukkan b : ";
   cin>>b;
   cout<<"Masukkan c : ";
   cin>>c;
   if(b < a){
      x = b;
      b = a;
      a = x;
   }
   if(c < a){
      x = c;
      c = a;
      a = x;
   }
   if(c < b){
      x = b;
      b = c;
      c = x;
   }
   cout<<"Bilangan setelah diurutkan : "<<a<<" "<<b<<" "<<c;
}
