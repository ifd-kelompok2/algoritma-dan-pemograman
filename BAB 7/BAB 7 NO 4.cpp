#include <iostream>
#include <math.h>
using namespace std;
main(){
    int a=0,b=0,c=0,ab=0;
    cout<<"Tulislah algoritma yang membaca panjang (integer) tiga buah sisi sebuah segitiga,";
    cout<<"a, b,dan c, yang dalam hal ini a<=b<=c, lalu tentukan apakah ketiga sisi tersebut";
    cout<<"membentuk segitiga siku-siku, segitiga lancip, atau segitiga tumpul"<<endl;
    cout<<"(petunjuk : gunuakan hukum phytagoras)"<<endl;
    cout<<"Masukan A : ";cin>>a;
    cout<<"Masukan B : ";cin>>b;
    cout<<"Masukan C : ";cin>>c;
    ab = (pow(a,2)) + (pow(b,2));
    if(c%5==0) c = 1 + pow(c,2);
    else c = pow(c,2);
    if(c == ab){
        cout<<"Segitiga Siku-siku";
    }
    if(c < ab){
        cout<<"Segitiga Lancip";
    }
    if(c > ab){
        cout<<"Segitiga Tumpul";
    }
   
}
