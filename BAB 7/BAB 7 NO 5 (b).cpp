#include <iostream>
#include <sstream>
using namespace std;
main(){
    int a;
    string b;
    cout<<"Tulislah algoritma yang membaca sebuah karakter digit ('9..0') lalu mengunversinya";
    cout<<"menjadi nilai integer (9..0). Misalnya, jika dibaca karakter '5', maka nilai konversi";
    cout<<"ke integer adalah 5. buatlah masing-masing algoritma untuk dua keadaan berikut :"<<endl;
    cout<<"(a) karakter digit yang dibaca diasumsukan sudah benar terletak dalam rentan'0'..'9'."<<endl;
    cout<<"(b) karakter yang dibaca mungkin bukan digit'0'..'9'. Jika karakter yang dibaca bukan";
    cout<<"karakter digit, maka hasil konvervinya di asumsikan bernilai -1."<<endl;
    cout<<"Masukan Karakter Angka (1..9) : "; cin>>b;
    istringstream convert(b);
    if(!(convert>>a)){
        a = -1;
    }
    if(a==1) cout<<a;
    else if(a==2) cout<<a;
    else if(a==3) cout<<a;
    else if(a==4) cout<<a;
    else if(a==5) cout<<a;
    else if(a==6) cout<<a;
    else if(a==7) cout<<a;
    else if(a==8) cout<<a;
    else if(a==9) cout<<a;
    else {
        a = -1;
        cout<<a;
    }
   
}
