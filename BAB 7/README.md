
# Berikut Soal-soal yang ada di BAB 7 :

1. Buatlah algoritma yang membaca sebuah bilangan bulat positif lalu menentukan apakah bilangan tersebut 
    merupakan kelipatan 4.

2. Pasar pasar swalayan X harga bagi pembeli yang nilai total belanjanya lebih dari Rp Rp100.000 titik Tulislah 
    algoritma untuk menentukan nilai belanja setelah dikurangi diskon. data masukan adalah nilai total
    belanja pembeli sedangkan keluarannya adalah diskon harga dan nilai belanjaannya setelah dikurangi diskon.

3. Tulislah algoritma yang membaca 3 buah bilangan bulat selalu mengurutkan 3 buah bilangan tersebut dari nilai 
    yang kecil ikan nila yang besar. keluaran adalah 3 buah bilangan yang terurut.

4. Tulis  algoritma yang membaca panjang ( integer)  3 buah sisi sebuah segitiga a, b dan c yang dalam hal ini  
    A< B < C , lalu menentukan apakah ketiga sisi tersebut membentuk segitiga siku-siku segitiga lancip 
    atau  segitiga tumpul ( petunjuk :  hukum Phytagoras).

5. Tulislah algoritma yang membaca sebuah karakter digit (‘ 0’ .. ‘9’)  selalu membuat mengkonversinya menjadi  
    nilai integer (0.. 9).  misalnya, jika dibaca karakter ‘5’, maka nilai konversinya ke  integer adalah 5 
    titik Buatlah masing-masing algoritma untuk dua keadaan berikut :
    - Karakter digit yang dibaca diasumsikan sudah benar terletak dalam rentang ‘ 0’ .. ‘9’
    - Karakter yang dibaca mungkin digigit ‘ 0’ .. ‘9’ titik jika karakter yang dibaca bukan karakter dikit 
      maka hasil konversinya diasumsikan bernilai -1.

6. jika kita berbelanja di pasar swalayan /supermarket nilai total belanja kita seringkali bukan kelipatan 
    pecahan rupiah yang berlaku titik misalnya, nilai total belanja adalah  Rp. 19.212,-. Andaikan saat ini   
    pecahan rupiah yang berlaku paling kecil Rp 25,-. Selain itu, juga ada pecahan Rp 50,- dan Rp100,-.  
    umumnya kasir pasar swalayan membulatkan nilai belanja ke pecahan yang terbesar menjadi  Rp.19.212, 
    dibulatkan menjadi  Rp 19. 225,- setitik hal ini jelas merugikan konsumen misalkan anda memiliki pasar 
    swalayan yang jujur dan tidak merugikan pembeli sehingga jika ada nilai belanja yang bukan kelipatan 
    pecahan yang ada, maka nilai belanja itu dibulatkan ke pecahan terendah.  Jadi,  Rp.19.212 ,- dibulatkan 
    menjadi Rp.19.200.   Tulislah algoritma yang membaca nilai belanja (  integer)  lalu membulatkannya ke 
    nilai uang dengan pecahan terendah.
7. Soal :
    (a) Tuliskan algoritma yang membaca bilangan bulat positif dalam rentang 1 sampai 10, lalu mengonversinya 
        ke dalam angka Romawi.
    (b) Kembangkan algoritma (a)  diatas sehingga dapat mengkonversi bilangan bulat positif sembarang kedalam 
        angka romawinya.
 
 8. Dalam bidang pengolahan Citra (image Processing) , elemen gambar terkecil disebut pixel (picture Element).  
    Nilai pixel untuk gambar 256   warna  adalah dari  0  sampai 255. Operasi-operasi terhadap pixel seringkali 
    berada di luar rentang nilai ini. jika ini kasusnya, maka nilai hasil operasi harus dipotong ( çlipping) 
    Sehingga tetap berada di dalam interval [0..25]. Jika nilai operasi lebih besar dari 255 maka nilai 
    tersebut dipotong menjadi 255 dan negatif maka dipotong menjadi 0. dibaca sebuah nilai hasil operasi 
    pengolahan Citra Buatlah algoritma untuk melakukan clipping tersebut.

9. Tinjau kembali soal latihan bab 6 Nomor 6 untuk menentukan berat badan ideal titik dibaca berat badan dan   
    tinggi badan seseorang. Tulislah pesan “Ideal” jika berat badan orang tersebut hanya berselisih kurang
    lebih 2 kg dari berat badan ideal, atau pesan “Tidak ideal”  jika tidak  berselisih kurang lebih 2 kg dari
    berat badan ideal titik Tulislah algoritmanya.

10. Tuliskan ke dalam bahasa pascal c dan C++  untuk algoritma soal nomor 1 sampai nomor 9 di atas.
    
11. Di baca 2 buah hari ,hari pertama dan hari kedua titik Tulislah algoritma untuk menghitung durasi ( 
    selisih)  kedua hari-hari tersebut. misalnya, hari pertama Kamis dan hari kedua Senin, maka dari Kamis ke 
    Senin durasinya 5 hari.

12. (versi lebih kompleks dari soal latihan nomor 3 bab 6)  dibaca Dua buah tanggal ( dd-mm-yyyy) Tulislah 
    algoritma untuk menghitung durasi hari antara kedua tanggal tersebut. sebagai contoh, tanggal 20-1-2015 dan 
    2-2-2015 durasinya 13 hari. di dalam algoritma ini anda harus memperhatikan tahun kabisat dan jumlah hari 
    yang berbeda pada setiap bulan.

13. Tulislah program Pascal/C/C++   yang membaca umur seseorang, lalu menentukan apakah umur termasuk kategori  
    balita (0-5), anak-anak (5-12), remaja (12-20), dewasa (20-60), atau tua (60  ke atas) .
    
14. Di sebuah maskapai penerbangan coma anak-anak yang berumur 12 tahun kebawah mendapatkan diskon tiket    
    sebesar 25%. Dibaca kuota keberangkatan, kota tujuan, nama penumpang tanggal lahir, dan tanggal 
    keberangkatan. harga tiket normal adalah x rupiah titik Tentukan Berapa harga tiket pesawat yang harus 
    dibayar dengan memperhatikan diskon jika calon penumpang berumur dibawah 12 tahun (  dihitung berdasarkan 
    tanggal lahir dan tanggal keberangkatan) 

