#include <iostream>
#include <conio.h>
using namespace std;

int jumlah, asc;

main(){
	int nilai[3];

    for(int i=0; i<3; i++){
        cout << "Nilai " << (i+1) << " : ";
        cin >> nilai[i];
    }

    for(int c=1;c<3;c++)
    {
        for(int d=0;d<3-c;d++)
        {
            if(nilai[d] > nilai[d+1])
            {
                asc=nilai[d];
                nilai[d]=nilai[d+1];
                nilai[d+1]=asc;
            }
        }
    }

    cout << endl << "Hasil Pengurutan Adalah";
    for(int i=0;i<3;i++)
    {
     cout << " " << nilai[i];
    }
    cout << endl;
    getch();
    return 0;
}