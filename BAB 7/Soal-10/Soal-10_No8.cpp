#include <iostream>
using namespace std;

int main(){
	int pixel;
	cout<<"Masukkan Nilai Pixel Gambar Anda: ";
	cin>>pixel;
	if(pixel>=0 && pixel<=255){
		cout<<"Maka Nilai Pixel Gambar Anda Tetap Sebesar "<<pixel<<endl;
	}
	else if (pixel>255){
		cout<<"Maka Nilai Pixel Gambar Anda Menjadi Sebesar 255\n";
	}
	else{
		cout<<"Maka Nilai Pixel Gambar Anda Menjadi 0\n";
	}
	return 0;
}