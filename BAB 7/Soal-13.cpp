#include <iostream>
using namespace std;

int main(){
	int umur;
	cout<<"Tulislah Program Pascal/C/C++ yang membaca umur seseorang berdasarkan kategorinya. Apakah seseorang termasuk kepada kategori balita(0-5), anak-anak(6-12), remaja (13-20), dewasa(20-60) dan tua (60 ke atas)."
	cout<<"Program Membaca Umur Seseorang\n\n";
	cout<<"Masukkan Umur: ";
	cin>>umur;
	
	if(umur>=0 && umur<=5){
		cout<<" Umur yang dimasukkan termasuk kategori balita\n";
	}
	else if(umur>=6 && umur<=12){
		cout<<" Umur yang dimasukkan termasuk kategori anak-anak\n";
	}
	else if(umur>=13 && umur<= 20){
		cout<<" Umur yang dimasukkan termasuk kategori remaja\n";
	}
	else if(umur>=21 && umur<=60){
		cout<<" Umur yang dimasukkan termasuk kategori dewasa\n";
	}
	else if(umur>60){
		cout<<" Umur yang dimasukkan termasuk kategori tua\n";

		}
	else{
		cout<<"Inputan Salah!\n";
	}
}
